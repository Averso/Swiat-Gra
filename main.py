import sys
from PySide import QtGui

from Interfejsy.Interfejs import Interfejs
from Swiat.Swiat import Swiat

app = QtGui.QApplication(sys.argv)

# wczytanie świata z xmla
sw = Swiat("test.xml")
inter = Interfejs("Świat", sw)
sw.add_interfejs(inter)


sys.exit(app.exec_())
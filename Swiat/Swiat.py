from xml.dom import minidom
from Organizmy.Zwierzeta.Antylopa import Antylopa
from Organizmy.Zwierzeta.Czlowiek import Czlowiek
from Organizmy.Zwierzeta.Lis import Lis
from Organizmy.Zwierzeta.Owca import Owca
from Organizmy.Zwierzeta.Wilk import Wilk
from Organizmy.Zwierzeta.Zolw import Zolw
from Organizmy.Rosliny.Guarana import Guarana
from Organizmy.Rosliny.Mlecz import Mlecz
from Organizmy.Rosliny.Trawa import Trawa
from Organizmy.Rosliny.WilczaJagoda import Wilcza_Jagoda




class Swiat:

    #KONSTRUKTOR
    def __init__(self, nazwa_pliku):

        self.organizmy = []
        self.__max_inicjatywa = None
        self.__wielkosc_pola = None

#        plik = open(nazwa_pliku,"r")
#        plik.close()
        DOM_Tree = minidom.parse(nazwa_pliku)
        swiat_xml = DOM_Tree.documentElement

        self.__szerokosc = int(swiat_xml.getAttribute("szerokosc"))
        self.__wysokosc = int(swiat_xml.getAttribute("wysokosc"))
        self.__bok = int(swiat_xml.getAttribute("szerKratki"))

        org_xml = swiat_xml.getElementsByTagName("organizm")

        self.__ilosc_organizmow = len(org_xml) - 1

        licznik_ludzi = 0


        for org in org_xml:
            id = int(org.getAttribute("id"))
            nazwa = org.getElementsByTagName("nazwa")[0].childNodes[0].data
            sila =  int(org.getElementsByTagName("sila")[0].childNodes[0].data)
            inicja = int(org.getElementsByTagName("inicjatywa")[0].childNodes[0].data)
            x = int(org.getElementsByTagName("x")[0].childNodes[0].data)
            y = int(org.getElementsByTagName("y")[0].childNodes[0].data)
            ruch = int(org.getElementsByTagName("ruch")[0].childNodes[0].data)

            if id == 0:
                self.organizmy.append(Wilk(id, nazwa, sila, inicja, x, y, ruch, self.__bok))
            elif id == 1:
                self.organizmy.append(Owca(id, nazwa, sila, inicja, x, y, ruch, self.__bok))
            elif id == 2:
                self.organizmy.append(Lis(id, nazwa, sila, inicja, x, y, ruch, self.__bok))
            elif id == 3:
                pancerz = int(org.getElementsByTagName("pancerz")[0].childNodes[0].data)
                pr_r = int(org.getElementsByTagName("prawdopodRuchu")[0].childNodes[0].data)
                self.organizmy.append(Zolw(id, nazwa, sila, inicja, x, y, ruch, self.__bok, pancerz, pr_r))
            elif id == 4:
                pr_u =int(org.getElementsByTagName("prawdUcieczki")[0].childNodes[0].data)
                self.organizmy.append(Antylopa(id, nazwa, sila, inicja, x, y, ruch, self.__bok, pr_u))
            elif id == 5:
                self.organizmy.append(Czlowiek(id, nazwa, sila, inicja, x, y, ruch, self.__bok))
                licznik_ludzi+=1
            elif id == 6:
                pr_z = int(org.getElementsByTagName("prawdopodZas")[0].childNodes[0].data)
                self.organizmy.append(Trawa(id, nazwa, sila, inicja, x, y, ruch, self.__bok, pr_z))
            elif id == 7:
                pr_z = int(org.getElementsByTagName("prawdopodZas")[0].childNodes[0].data)
                proby = int(org.getElementsByTagName("proby")[0].childNodes[0].data)
                self.organizmy.append(Mlecz(id, nazwa, sila, inicja, x, y, ruch, self.__bok, pr_z, proby))
            elif id == 8:
                pr_z = int(org.getElementsByTagName("prawdopodZas")[0].childNodes[0].data)
                bonus = int(org.getElementsByTagName("bonus")[0].childNodes[0].data)
                self.organizmy.append(Guarana(id, nazwa, sila, inicja, x, y, ruch, self.__bok, pr_z, bonus))
            elif id == 9:
                pr_z = int(org.getElementsByTagName("prawdopodZas")[0].childNodes[0].data)
                self.organizmy.append(Wilcza_Jagoda(id, nazwa, sila, inicja, x, y, ruch, self.__bok, pr_z))

            if licznik_ludzi >= 2:
                assert("Czlowiek moze byc tylko 1!!")

            self.__max_inicjatywa = 7



    def wykonaj_ture(self):
        for i in range(self.__max_inicjatywa, -1, -1):
           for j in range(0, self.__ilosc_organizmow):
                if self.organizmy[j].get_inicjatywa() == i and not(self.organizmy[j].get_martwy()):
                     self.organizmy[j].akcja(self,j)


        self._pogrzeb_martwe()


    def dodaj_nowe(self,rodzic,x,y):
        id = rodzic.get_id()

        if id == 0:
                self.organizmy.append(Wilk(id, "Wilk", 9, 5, 0, x, y, 1, self.__bok))
        elif id == 1:
                self.organizmy.append(Owca(id, "Owca", 4, 4, 1, x, y, 1, self.__bok))
        elif id == 2:
                self.organizmy.append(Lis(id, "Lis", 3, 7, 2, x, y, 1, self.__bok))
        elif id == 3:
                self.organizmy.append(Zolw(id, "Zolw", 2, 1, 3, x, y, 1, self.__bok, 5, 75))
        elif id == 4:
                self.organizmy.append(Antylopa(id,"Antylopa", 4, 4, 4, 4, x, y, 1, self.__bok, 50))
        elif id == 5:
                self.organizmy.append(Czlowiek(id,"Czlowiek", 5, 4, 5, x, y, 1, self.__bok))
        elif id == 6:
                self.organizmy.append(Trawa(id, "Trawa", 0, 0,6, x, y, 1, self.__bok, 4))
        elif id == 7:
                self.organizmy.append(Mlecz(id, "Mlecz", 0, 0, 7, x, y, 1, self.__bok, 4, 3))
        elif id == 8:
                self.organizmy.append(Guarana(id, "Guarana", 0, 0, 8, x, y, 1, self.__bok, 4, 3))
        elif id == 9:
                self.organizmy.append(Wilcza_Jagoda(id, "WilczaJagoda", 99, 0, 9, x, y, 1, self.__bok, 4))


    def _pogrzeb_martwe(self):

        do_usuniecia = []

        for i in range (0,len(self.organizmy)):
            if (self.organizmy[i].get_martwy()):
                do_usuniecia.append(i)

        #usuwamy indeksy od tylu
        for i in reversed(do_usuniecia):
             del self.organizmy[i]

        self.__ilosc_organizmow = len(self.organizmy)


    def get_wysokosc(self):
        return self. __wysokosc

    def get_szerokosc(self):
        return self.__szerokosc

    def get_ilosc_organizmow(self):
        return self.__ilosc_organizmow

    def get_bok(self):
        return self.__bok

    def add_interfejs(self,i):
        self.interfejs = i
from Organizmy.Roslina import Roslina

class Guarana(Roslina):

    __bonus = None

    def __init__(self, id, nazwa, sila, inicjatywa, x, y, ruch, bok, prawdo_z, bonus):
        self.set_id(id)
        self.set_nazwa(nazwa)
        self.set_sila(sila)
        self.set_inicjatywa(inicjatywa)
        self.set_x(x)
        self.set_y(y)
        self.set_ruch(ruch)
        self.set_bok(bok)
        self.set_prawdopodob_zas(prawdo_z)
        self.set_martwy(False)
        self.set_czy_sie_rusza(True)
        self.set_bonus(bonus)
        self.set_obrazek("obrazki/guarana.png")

    def set_bonus(self,bonus):
        self.__bonus=bonus

    def get_bonus(self):
        return self.__bonus

    def kolizja(self,s,indeks):
        atakujacy = s.organizmy[indeks]

        atakujacy.set_sila(atakujacy.get_sila() + self.get_bonus())
        s.interfejs.log.append(atakujacy.get_nazwa() + " zjadl " + self.get_nazwa() + "; Sila " + atakujacy.get_nazwa() + " rosnie o " + str(self.get_bonus()))
        self.set_martwy(True)


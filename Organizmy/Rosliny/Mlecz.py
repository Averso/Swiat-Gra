from Organizmy.Roslina import Roslina


class Mlecz(Roslina):

    __proby = None

    def __init__(self, id, nazwa, sila, inicjatywa,x, y, ruch, bok, prawdo_z, proby):
        self.set_id(id)
        self.set_nazwa(nazwa)
        self.set_sila(sila)
        self.set_inicjatywa(inicjatywa)
        self.set_x(x)
        self.set_y(y)
        self.set_ruch(ruch)
        self.set_bok(bok)
        self.set_prawdopodob_zas(prawdo_z)
        self.set_martwy(False)
        self.set_czy_sie_rusza(True)
        self.set_proby(proby)
        self.set_obrazek("obrazki/mlecz.png")

    def akcja(self,s,indeks):
        for i in range(0,self.get_proby()):
            los = self._losuj_czy_zasialo()

            if los < self.get_prawdopodob_zas():
                los = self._losuj_ruch()

                if los == 0:
                    if self.czy_wolne_pole(s, self.get_x(), self.get_y() - self.get_ruch()):
                        s.dodaj_nowe(self, self.get_x(), self.get_y() - self.get_ruch())
                        s.interfejs.log.append(self.get_nazwa() + " rozsial sie")
                elif los == 1:
                    if self.czy_wolne_pole(s, self.get_x() + self.get_ruch(), self.get_y()):
                        s.dodaj_nowe(self, self.get_x() + self.get_ruch(), self.get_y())
                        s.interfejs.log.append(self.get_nazwa() + " rozsial sie")
                elif los == 2:
                    if self.czy_wolne_pole(s, self.get_x(), self.get_y() + self.get_ruch()):
                        s.dodaj_nowe(self, self.get_x(), self.get_y() + self.get_ruch())
                        s.interfejs.log.append(self.get_nazwa() + " rozsial sie")
                elif los == 3:
                    if self.czy_wolne_pole(s, self.get_x() - self.get_ruch(), self.get_y()):
                        s.dodaj_nowe(self, self.get_x() - self.get_ruch(), self.get_y())
                        s.interfejs.log.append(self.get_nazwa() + " rozsial sie")


    #ustawianie/pobieranie wartosci
    def set_proby(self,proby):
        self.__proby = proby

    def get_proby(self):
        return self.__proby
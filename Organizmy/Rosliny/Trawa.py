from Organizmy.Roslina import Roslina


class Trawa(Roslina):
    def __init__(self, id, nazwa, sila, inicjatywa, x, y, ruch, bok, prawdo_z):
        self.set_id(id)
        self.set_nazwa(nazwa)
        self.set_sila(sila)
        self.set_inicjatywa(inicjatywa)
        self.set_x(x)
        self.set_y(y)
        self.set_ruch(ruch)
        self.set_bok(bok)
        self.set_prawdopodob_zas(prawdo_z)
        self.set_martwy(False)
        self.set_czy_sie_rusza(True)
        self.set_obrazek("obrazki/trawa.png")
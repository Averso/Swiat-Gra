from Organizmy.Roslina import Roslina

class Wilcza_Jagoda(Roslina):
    def __init__(self, id, nazwa, sila, inicjatywa, x, y, ruch, bok, prawdo_z):
        self.set_id(id)
        self.set_nazwa(nazwa)
        self.set_sila(sila)
        self.set_inicjatywa(inicjatywa)
        self.set_x(x)
        self.set_y(y)
        self.set_ruch(ruch)
        self.set_bok(bok)
        self.set_prawdopodob_zas(prawdo_z)
        self.set_martwy(False)
        self.set_czy_sie_rusza(True)
        self.set_obrazek("obrazki/wilczajagoda.png")

    def kolizja(self,s,indeks):
        atakujacy = s.organizmy[indeks]
        if (atakujacy.get_id() == 5) and (atakujacy.czy_niesmiertelny):
            s.interfejs.log.append(atakujacy.get_nazwa() + " jest niesmiertelny i przezyl zjedzenie " + self.get_nazwa())
            self.set_martwy(True)
        else:
             s.interfejs.log.append(atakujacy.get_nazwa() + " zjadl " + self.get_nazwa() + "; " + atakujacy.get_nazwa() + " umiera")
             atakujacy.set_martwy(True)

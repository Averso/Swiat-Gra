from random import randint
from Organizmy.Zwierze import Zwierze


class Zolw(Zwierze):

    def __init__(self, id, nazwa, sila, inicjatywa, x, y, ruch, bok, pancerz, prawd_r):
        self.set_id(id)
        self.set_sila(sila)
        self.set_nazwa(nazwa)
        self.set_inicjatywa(inicjatywa)
        self.set_x(x)
        self.set_y(y)
        self.set_ruch(ruch)
        self.set_bok(bok)
        self.set_martwy(False)
        self.set_czy_sie_rusza(True)
        self.__pancerz = pancerz
        self.__prawdopod_ruchu = prawd_r
        self.set_obrazek("obrazki/zolw.png")


    def _losuj_czy_sie_rusza(self):
        return randint(1,100)

    def akcja(self,s,indeks):
        los = self._losuj_czy_sie_rusza()

        if los >= self.__prawdopod_ruchu:
            self.set_czy_sie_rusza(True)
            los = self._losuj_ruch()

            if los == 0:
                self.ruch(s,indeks,self.get_x(), self.get_y() - self.get_ruch())
            elif los == 1:
                self.ruch(s,indeks,self.get_x() + self.get_ruch(), self.get_y())
            elif los == 2:
                self.ruch(s,indeks,self.get_x(), self.get_y() + self.get_ruch())
            elif los == 3:
                self.ruch(s,indeks,self.get_x() - self.get_ruch(), self.get_y())

    def kolizja(self,s,indeks):
       atakujacy = s.organizmy[indeks]

       if self.get_id() == atakujacy.get_id():
           atakujacy.set_czy_sie_rusza(False)

           los = self._losuj_ruch()
           czy_rozmnozyl_sie = False
           if los == 0:
               if self.czy_wolne_pole(s, self.get_x(), self.get_y() - self.get_ruch()):
                   s.dodaj_nowe(self, self.get_x(), self.get_y() - self.get_ruch())
                   czy_rozmnozyl_sie = True
           elif los == 1:
               if self.czy_wolne_pole(s,self.get_x() + self.get_ruch(), self.get_y()):
                    s.dodaj_nowe(self, self.get_x() + self.get_ruch(), self.get_y())
                    czy_rozmnozyl_sie = True
           elif los == 2:
               if self.czy_wolne_pole(s,self.get_x(), self.get_y() + self.get_ruch()):
                    s.dodaj_nowe(self, self.get_x(), self.get_y() + self.get_ruch())
                    czy_rozmnozyl_sie = True
           elif los == 3:
               if self.czy_wolne_pole(s,self.get_x() - self.get_ruch(), self.get_y()):
                    s.dodaj_nowe(self,self.get_x() - self.get_ruch(), self.get_y())
                    czy_rozmnozyl_sie = True

           if czy_rozmnozyl_sie:
                s.interfejs.log.append(self.get_nazwa() + " rozmnozyl sie z " + atakujacy.get_nazwa())

       else:
           if self.get_sila() >= atakujacy.get_sila():
                if atakujacy.get_id == 5 and atakujacy.get_czy_niesmiertelny():
                       self.ucieczka(s)
                       s.interfejs.log.append(self.get_nazwa() + " uciekla w poplochu przed " + atakujacy.get_nazwa())
                else:
                       s.interfejs.log.append(self.get_nazwa() + " zjadla " + atakujacy.get_nazwa())
                       atakujacy.set_martwy(True)
           else:
                s.interfejs.log.append(atakujacy.get_nazwa() + " zjadl " + self.get_nazwa())
                self.set_martwy(True)

from Organizmy.Zwierze import Zwierze


class Czlowiek(Zwierze):
    cooldown = 0
    czas_trwania = 0
    czy_dostepna = True
    czy_niesmiertelny = False
    kierunek = None

    def __init__(self, id, nazwa, sila, inicjatywa, x, y, ruch, bok):
        self.set_id(id)
        self.set_sila(sila)
        self.set_nazwa(nazwa)
        self.set_inicjatywa(inicjatywa)
        self.set_x(x)
        self.set_y(y)
        self.set_ruch(ruch)
        self.set_bok(bok)
        self.set_martwy(False)
        self.set_czy_sie_rusza(True)
        self.set_obrazek("obrazki/czlowiek.gif")


    def akcja(self,s,indeks):
        self.set_czy_sie_rusza(True)

        if self.czy_niesmiertelny:
            if self.czas_trwania != 0:
                self.czas_trwania-=1
            else:
                self.czy_niesmiertelny = False
                self.cooldown = 5
                s.interfejs.log.append(self.get_nazwa() + ": koniec niesmiertelnosci")
        else:
            if self.cooldown != 0:
                self.cooldown -= 1
            else:
                self.czy_dostepna = True

        if self.kierunek == 0:
            self.ruch(s, indeks, self.get_x(), self.get_y() - self.get_ruch())
        elif self.kierunek == 1:
            self.ruch(s, indeks, self.get_x() - self.get_ruch(), self.get_y())
        elif self.kierunek == 2:
            self.ruch(s, indeks, self.get_x() + self.get_ruch(), self.get_y())
        elif self.kierunek == 3:
            self.ruch(s, indeks, self.get_x(), self.get_y() + self.get_ruch())
        else:
            pass


    def kolizja(self,s,indeks):
        atakujacy = s.organizmy[indeks]
        if not self.czy_dostepna:
            if self.get_sila() >= atakujacy.get_sila():
                s.interfejs.log.append(self.get_nazwa() + " zjadl " + atakujacy.get_nazwa())
                atakujacy.set_martwy(True)
            else:
                atakujacy.set_czy_sie_rusza(False)
        else:
            if self.get_sila() >= atakujacy.get_sila():
                s.interfejs.log.append(self.get_nazwa() + " zjadl " + atakujacy.get_nazwa())
                atakujacy.set_martwy(True)
            else:
                s.interfejs.log.append(atakujacy.get_nazwa() + " zjadl " + self.get_nazwa())
                self.set_martwy(True)

    def aktywuj_umiejetnosc(self):
        self.czy_dostepna = False
        self.czy_niesmiertelny = True
        self.czas_trwania = 5

    def ruch(self,s,indeks,x,y):
        if (x < 0) or (x>s.get_szerokosc()-1) or (y<0) or (y>s.get_wysokosc()-1):
            pass
        else:
            right = self.czy_jest_inny_organizm(s,x,y)
            if(right != None): #jak na polu jest organizm
                right.kolizja(s,indeks)

                if not self.get_martwy() and self.get_czy_sie_rusza():
                    self.set_x(x)
                    self.set_y(y)

            else:
                self.set_x(x)
                self.set_y(y)


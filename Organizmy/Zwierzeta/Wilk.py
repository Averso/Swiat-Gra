from Organizmy.Zwierze import Zwierze


class Wilk(Zwierze):

    def __init__(self, id, nazwa, sila, inicjatywa,x, y, ruch, bok):
        self.set_id(id)
        self.set_sila(sila)
        self.set_nazwa(nazwa)
        self.set_inicjatywa(inicjatywa)
        self.set_x(x)
        self.set_y(y)
        self.set_ruch(ruch)
        self.set_bok(bok)
        self.set_martwy(False)
        self.set_czy_sie_rusza(True)
        self.set_obrazek("obrazki/wilk.png")
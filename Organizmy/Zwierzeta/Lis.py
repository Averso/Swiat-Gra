from Organizmy.Zwierze import Zwierze

class Lis(Zwierze):
    def __init__(self, id, nazwa, sila, inicjatywa, x, y, ruch, bok):
        self.set_id(id)
        self.set_sila(sila)
        self.set_nazwa(nazwa)
        self.set_inicjatywa(inicjatywa)
        self.set_x(x)
        self.set_y(y)
        self.set_ruch(ruch)
        self.set_bok(bok)
        self.set_martwy(False)
        self.set_czy_sie_rusza(True)
        self.set_obrazek("obrazki/lis.png")

    def ruch(self,s,indeks,x,y):
        if (x < 0) or (x > s.get_szerokosc() - 1) or (y > s.get_wysokosc() - 1) or (y < 0):
            self.akcja(s, indeks)
        elif self.wyniuchaj_silniejsze_zwierze(s,x,y):
            self.akcja(s, indeks)
        else:

            right = self.czy_jest_inny_organizm(s,x,y)
            if right != None:
                right.kolizja(s,indeks)

                if not(self.get_martwy()) and self.get_czy_sie_rusza():
                    self.set_x(x)
                    self.set_y(y)
            else:
                self.set_x(x)
                self.set_y(y)

    def wyniuchaj_silniejsze_zwierze(self,s, x, y):
        for org in s.organizmy:
            if org.get_x() == x and org.get_y() == y and self.get_sila() < org.get_sila():
                return True
        return False
from random import randint
from Organizmy.Organizm import Organizm


class Roslina(Organizm):

    __prawdopodob_zas = None

    def akcja(self,s,indeks):
       los = self._losuj_czy_zasialo()
       if los < self.get_prawdopodob_zas():
           los = self._losuj_ruch()

           if los == 0:
               if self.czy_wolne_pole(s, self.get_x(), self.get_y() - self.get_ruch()):
                   s.dodaj_nowe(self, self.get_x(), self.get_y() - self.get_ruch())
                   s.interfejs.log.append(self.get_nazwa() + " rozsiala sie")
           elif los == 1:
               if self.czy_wolne_pole(s, self.get_x() + self.get_ruch(), self.get_y()):
                   s.dodaj_nowe(self, self.get_x() + self.get_ruch(), self.get_y())
                   s.interfejs.log.append(self.get_nazwa() + " rozsiala sie")
           elif los == 2:
               if self.czy_wolne_pole(s, self.get_x(), self.get_y() + self.get_ruch()):
                   s.dodaj_nowe(self, self.get_x(), self.get_y() + self.get_ruch())
                   s.interfejs.log.append(self.get_nazwa() + " rozsiala sie")
           elif los == 3:
               if self.czy_wolne_pole(s, self.get_x() - self.get_ruch(), self.get_y()):
                   s.dodaj_nowe(self, self.get_x() - self.get_ruch(), self.get_y())
                   s.interfejs.log.append(self.get_nazwa() + " rozsiala sie")

    def kolizja(self,s,indeks):
        atakujacy = s.organizmy[indeks]

        s.interfejs.log.append(atakujacy.get_nazwa() + " zjadl " + self.get_nazwa())
        self.set_martwy(True)

    def _losuj_czy_zasialo(self):
        return randint(1,100)

    #pobieranie/ustawianie wartosci
    def get_prawdopodob_zas(self):
        return self.__prawdopodob_zas

    def set_prawdopodob_zas(self,pra):
        self.__prawdopodob_zas = pra
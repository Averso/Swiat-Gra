from Organizmy.Organizm import Organizm


class Zwierze(Organizm):

    def czy_jest_inny_organizm(self,s,x,y):
        for org in s.organizmy:
           if (org.get_x() == x) and (org.get_y() == y):
               return org
        return None

    def ruch(self,s,indeks,x,y):
        if (x < 0) or (x>s.get_szerokosc()-1) or (y<0) or (y>s.get_wysokosc()-1):
            self.akcja(s,indeks)
        else:
            right = self.czy_jest_inny_organizm(s,x,y)
            if(right != None): #jak na polu jest organizm
                right.kolizja(s,indeks)

                if not self.get_martwy() and self.get_czy_sie_rusza():
                    self.set_x(x)
                    self.set_y(y)

            else:
                self.set_x(x)
                self.set_y(y)

    def akcja(self,s,indeks):
        self.set_czy_sie_rusza(True)
        los = self._losuj_ruch()
        if los == 0:
            self.ruch(s,indeks,self.get_x(), self.get_y() - self.get_ruch())
        elif los == 1:
            self.ruch(s,indeks,self.get_x() + self.get_ruch(), self.get_y())
        elif los == 2:
            self.ruch(s,indeks,self.get_x(), self.get_y() + self.get_ruch())
        elif los == 3:
            self.ruch(s,indeks,self.get_x() - self.get_ruch(), self.get_y())

    def kolizja(self,s,indeks):
       atakujacy = s.organizmy[indeks]

       if self.get_id() == atakujacy.get_id():
           atakujacy.set_czy_sie_rusza(False)

           los = self._losuj_ruch()
           czy_rozmnozyl_sie = False
           if los == 0:
               if self.czy_wolne_pole(s, self.get_x(), self.get_y() - self.get_ruch()):
                   s.dodaj_nowe(self, self.get_x(), self.get_y() - self.get_ruch())
                   czy_rozmnozyl_sie = True
           elif los == 1:
               if self.czy_wolne_pole(s,self.get_x() + self.get_ruch(), self.get_y()):
                    s.dodaj_nowe(self, self.get_x() + self.get_ruch(), self.get_y())
                    czy_rozmnozyl_sie = True
           elif los == 2:
               if self.czy_wolne_pole(s,self.get_x(), self.get_y() + self.get_ruch()):
                    s.dodaj_nowe(self, self.get_x(), self.get_y() + self.get_ruch())
                    czy_rozmnozyl_sie = True
           elif los == 3:
               if self.czy_wolne_pole(s,self.get_x() - self.get_ruch(), self.get_y()):
                    s.dodaj_nowe(self,self.get_x() - self.get_ruch(), self.get_y())
                    czy_rozmnozyl_sie = True

           if czy_rozmnozyl_sie:
                s.interfejs.log.append(self.get_nazwa() + " rozmnozyl sie z " + atakujacy.get_nazwa())

       else:
           if self.get_sila() >= atakujacy.get_sila():
               if(atakujacy.get_id() == 5) and (atakujacy.get_czy_niesmiertelny()):
                   self.ucieczka(s)
                   s.interfejs.log.append(self.get_nazwa() + " uciekl w poplochu przed " + atakujacy.get_nazwa())
               else:
                    s.interfejs.log.append(self.get_nazwa() + " zjadl " + atakujacy.get_nazwa())
                    atakujacy.set_martwy(True)
           else:
                s.interfejs.log.append(atakujacy.get_nazwa() + " zjadl " + self.get_nazwa())
                self.set_martwy(True)

    def ucieczka(self,s):
        if self.czy_wolne_pole(s, self.get_x() - self.get_ruch(), self.get_y()):
            self.set_x(self.get_x() - self.get_ruch())
        elif self.czy_wolne_pole(s, self.get_x() + self.get_ruch(), self.get_y()):
            self.set_x(self.get_x() + self.get_ruch())
        elif self.czy_wolne_pole(s,self.get_x(), self.get_y() - self.get_ruch()):
            self.set_y(self.get_y() - self.get_ruch())
        elif self.czy_wolne_pole(s,self.get_x(), self.get_y() + self.get_ruch()):
            self.set_y(self.get_y() + self.get_ruch())






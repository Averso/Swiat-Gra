from abc import ABCMeta, abstractmethod
from random import randint
from PySide import QtGui

class Organizm(metaclass=ABCMeta):

    __id = None #id organizmu
    __nazwa = None #nazwa organizmy
    __sila = None
    __inicjatywa = None
    __x = None
    __y = None
    __martwy = None
    __ruch = None
    __czy_sie_rusza = None
    __bok = None
    __obrazek = None #jako string

    @abstractmethod
    def akcja(self,s,indeks):
        pass

    @abstractmethod
    def kolizja(self,s,indeks):
        pass

    def rysowanie(self, qp):
        obraz = QtGui.QImage(self.get_obrazek())
        qp.drawImage(self.get_x()*self.get_bok(),self.get_y()*self.get_bok(), obraz)

    def _losuj_ruch(self):
        rand = randint(0,3)
        return rand

    def czy_wolne_pole(self,s,x,y):
        if (x < 0) or (x>s.get_szerokosc()-1) or (y<0) or (y>s.get_wysokosc()-1):
            return False

        for i in range (0,s.get_ilosc_organizmow()):
           if (s.organizmy[i].get_x() == x ) and (s.organizmy[i].get_y() == y ):
               return False

        return True



    #GETTERY
    def get_nazwa(self):
        return self.__nazwa

    def get_sila(self):
        return self.__sila

    def get_x(self):
        return self.__x

    def get_y(self):
        return self.__y

    def get_martwy(self):
        return self.__martwy

    def get_id(self):
        return self.__id

    def get_inicjatywa(self):
        return self.__inicjatywa

    def get_ruch(self):
        return self.__ruch

    def get_czy_sie_rusza(self):
        return self.__czy_sie_rusza

    def get_bok(self):
        return self.__bok

    def get_obrazek(self):
        return self.__obrazek

    #SETTERY

    def set_nazwa(self,nazwa):
        self.__nazwa = nazwa

    def set_sila(self, sila):
        self.__sila = sila

    def set_x(self, x):
        self.__x = x

    def set_y(self, y):
        self.__y = y

    def set_martwy(self, martwy):
        self.__martwy = martwy

    def set_id(self, id):
        self.__id = id

    def set_inicjatywa(self, inicjatywa):
        self.__inicjatywa = inicjatywa

    def set_ruch(self, ruch):
        self.__ruch = ruch

    def set_czy_sie_rusza(self, czy):
        self.__czy_sie_rusza = czy

    def set_bok(self, bok):
        self.__bok = bok

    def set_obrazek(self,obrazek):
        self.__obrazek = obrazek
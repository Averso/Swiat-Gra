from PySide import QtGui,QtCore

from Interfejsy.Plansza import Plansza

class Interfejs(QtGui.QWidget):

     __czy_mozna = True #czy można iść w wybranym kierunku

     #konstruktor z podaniem tytulu okna i przypisaniem świata
     def __init__(self, tytul, s):
        super(Interfejs, self).__init__()
        self.initUI(tytul, s)

     #inicjalizacja interfejsu
     def initUI(self, tytul, s):
         #przycisk następnej tury
        self.turaButton = QtGui.QPushButton("Nastepna Tura")
        self.turaButton.setFocusPolicy(QtCore.Qt.NoFocus)
        self.turaButton.clicked.connect(self.turaClicked)

        self.log = QtGui.QTextBrowser()
        self.log.setOpenLinks(False)
        self.log.setFocusPolicy(QtCore.Qt.NoFocus)


        self.__swiat = s
        for org in self.__swiat.organizmy:
           if org.get_id() == 5:
              self.__czlowiek = org

        w = self.__swiat.get_szerokosc()*self.__swiat.get_bok()
        h = self.__swiat.get_wysokosc()*self.__swiat.get_bok()

        self.plansza = Plansza(w, h, s)
        vbox = QtGui.QVBoxLayout()
        vbox.addStretch(1)
        vbox.addWidget(self.plansza)
        vbox.addWidget(self.turaButton)
        vbox.addWidget(self.log)

        self.setLayout(vbox)
        self.setMinimumSize(w,h)
        self.setWindowTitle(tytul)
        self.show()

     def turaClicked(self):
        sender = self.sender()
        if self.__czy_mozna:
            self.__swiat.wykonaj_ture()
        self.update()



     def keyPressEvent(self, k):


        if k.key() == QtCore.Qt.Key_Up:
            if ((self.__czlowiek.get_y() - self.__czlowiek.get_ruch()) < 0):
                self.log.append(self.__czlowiek.get_nazwa() + " nie moze isc w gore, wybierz inny kierunek.")
                self.__czy_mozna = False
            else:
                self.__czlowiek.kierunek = 0
                self.log.append(self.__czlowiek.get_nazwa() + " pojdzie w gore.")
                self.__czy_mozna = True
        elif k.key() == QtCore.Qt.Key_Left:
            if ((self.__czlowiek.get_x() - self.__czlowiek.get_ruch()) < 0):
                self.log.append(self.__czlowiek.get_nazwa() + " nie moze isc w lewo, wybierz inny kierunek.")
                self.__czy_mozna = False
            else:
                self.__czlowiek.kierunek = 1
                self.log.append(self.__czlowiek.get_nazwa() + " pojdzie w lewo.")
                self.__czy_mozna = True
        elif k.key() == QtCore.Qt.Key_Right:
            if ((self.__czlowiek.get_x() + self.__czlowiek.get_ruch()) > (self.__swiat.get_szerokosc() - 1)):
                self.log.append(self.__czlowiek.get_nazwa() + " nie moze isc w prawo, wybierz inny kierunek.")
                self.__czy_mozna = False
            else:
                self.__czlowiek.kierunek = 2
                self.log.append(self.__czlowiek.get_nazwa() + " pojdzie w prawo.")
                self.__czy_mozna = True
        elif k.key() == QtCore.Qt.Key_Down:
            if ((self.__czlowiek.get_y() + self.__czlowiek.get_ruch()) > (self.__swiat.get_wysokosc() - 1)):
                self.log.append(self.__czlowiek.get_nazwa() + " nie moze isc w dol wybierz inny kierunek.")
                self.__czy_mozna = False
            else:
                self.__czlowiek.kierunek = 3
                self.log.append(self.__czlowiek.get_nazwa() + " pojdzie w dol.")
                self.__czy_mozna = True
        elif k.key() == QtCore.Qt.Key_U:
            if self.__czlowiek.czy_dostepna:
                self.__czlowiek.aktywuj_umiejetnosc()
                self.log.append(self.__czlowiek.get_nazwa() +": Aktywowano umiejetnosc.")
            else:
                 self.log.append(self.__czlowiek.get_nazwa() +": Umiejetnosc niedostepna")
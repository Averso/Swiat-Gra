from PySide import QtGui, QtCore

class Plansza(QtGui.QFrame):
    def __init__(self,w,h,s):
        super(Plansza, self).__init__()
        self.initUI(w, h, s)

    def initUI(self, w, h,s):
        self.setMinimumSize(w,h)
        self.setMaximumSize(w,h)
        self.__swiat = s
        self.setFocusPolicy(QtCore.Qt.NoFocus)
        self.ziemiaTex = QtGui.QImage("obrazki/ziemia.png")

    def rysuj_plansze(self, qp):
        color = QtGui.QColor(0, 0, 0)
        qp.setPen(color)
        #qp.setBrush(QtGui.QColor(87,76,60))
        #qp.drawRect(0, 0, self.__swiat.get_szerokosc()*self.__swiat.get_bok(), self.__swiat.get_wysokosc()*self.__swiat.get_bok())

        for i in range(0, self.__swiat.get_wysokosc()):
           for j in range(0, self.__swiat.get_szerokosc()):
              qp.drawImage(i*self.__swiat.get_bok(), j*self.__swiat.get_bok(), self.ziemiaTex)

        for org in self.__swiat.organizmy:
             org.rysowanie(qp)

    def paintEvent(self, e):
        qp = QtGui.QPainter()
        qp.begin(self)
        self.rysuj_plansze(qp)
        qp.end()
